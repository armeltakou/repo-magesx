# -*- coding: utf-8 -*-

from odoo import fields, models




class IotConnecteur(models.Model):
    _name = "iot.connecteur"
    _description = "Connecteur Médecin - Patient"
    _rec_name = "url_api"


    #idconnecteur = fields.Integer(string="ID Connecteur", default=1)
    #iduser = fields.Many2one("iot.utilisateurs", string="ID Utilisateur")
    url_api = fields.Char(string="URL de l'API", default="https://account.huami.com/v2/client/login")
      

    def name_get(self):
        res=[]
        for field in self:
            res.append((field.id, field.token))           
        return res 
    