# -*- coding: utf-8 -*-

from odoo import fields, models, api
import argparse
import requests
import urllib.parse
import json
import base64
import datetime
from odoo.exceptions import ValidationError


class IotUtilisateur(models.Model):
    _name = "iot.utilisateurs"
    _description = "Patient/Médecin du système"
    _rec_name = "pseudo"

    iduser = fields.Char(string="ID Patient", readonly=True)
    nom = fields.Char(string="Nom")
    prenom = fields.Char(string="Prenom")
    pseudo = fields.Char(string="Pseudo", required=True)
    sexe = fields.Selection(
        selection=[('H', 'Homme'), ('F', 'Femme')], string='Genre H/F')
    datenaissance = fields.Char(string="Date de naissance")
    contact = fields.Char(string="Contact")
    email = fields.Char(string="Email")
    state = fields.Selection(selection=[('draft', 'Brouillon'), ('done', 'Connecter'), ('fail', 'Echec')],
                             default="draft", string='etat')
    # token = fields.Char(string="Token", readonly=True)
    # contry_code = fields.Char(string="country code")
    # app_token = fields.Char(string="App token", required=True)
    # app_ttl = fields.Integer(string="App ttl", required=True)
    # login_token = fields.Char(string="Login token", required=True)
    # ttl = fields.Integer(string="ttl", required=True)
    idmedecin = fields.Many2one("res.users", string="Medecin")


    activities = fields.One2many("iot.activite", "patient_id", string="Activities")
    connecteurs = fields.Char(string="URL de l'API", default="https://account.huami.com/v2/client/login", readonly=True)
    @api.model
    def _run_patient_activities(self):
        records = self.search([])
        for patient in records:
            connexion = patient.idmedecin
            data = connexion.mifit_auth_email(connexion.email, connexion.mifitpassword)
            activity_data = connexion.mifit_get_history_patients(data, patient.iduser)
            for activity in activity_data:
                intdate = activity['date']
                timestamp = datetime.datetime.fromtimestamp(intdate)
                if self.env['iot.activite'].search([('date','=',timestamp), ('patient_id','=',patient.id)]):
                     continue
                activity.update({
                    'patient_id': patient.id,
                    'date': timestamp
                })
                self.env['iot.activite'].create(activity)

    