# -*- coding: utf-8 -*-

from odoo import fields, models
from odoo import fields, models
import argparse
import requests
import urllib.parse
import json
import base64
import datetime
from odoo.exceptions import ValidationError



class IotResusers(models.Model):
    _inherit = "res.users"
    _description = "Medecin"

    mifitiduser = fields.Char(string="ID Medecin", readonly=True, size=15)
    app_token = fields.Char(string="App token Medecin", readonly=True)
    app_ttl = fields.Integer(string="App ttl Medecin", readonly=True)
    login_token = fields.Char(string="Login token Medecin", readonly=True)
    ttl = fields.Integer(string="ttl Medecin", readonly=True)
    mifitpassword = fields.Char(string="Mot de passe Medecin", required=True)
    email = fields.Char(string="Email", required=True)
    

    
    def action_connect_api(self):
        for record in self:
            data = record.mifit_auth_email(record.email, record.mifitpassword)
            record.app_token = data['token_info']['app_token']
            record.app_ttl = data['token_info']['app_ttl']
            record.mifitiduser = data['token_info']['user_id']
    def action_reconnect_api(self):
        for record in self:
            data = record.mifit_auth_email(record.email, record.mifitpassword)
            record.mifit_get_users_patients(data)
            
            
    # def name_get(self):
    #     res = []
    #     for field in self:
    #         name = field.nom + ' ' + field.prenom
    #         res.append((field.id, name))
    #     return res

    def get_band_data(self, auth_info):
        print("Retrieveing mi band data")
        band_data_url = 'https://api-mifit-us2.huami.com/v1/data/band_data.json?r=4b5ec474-5b8b-47fb-8c0e-a5bb64139877&t=1655575560695&userid=3084457297&appid=428135909242707968&byteLength=8&callid=1655575560690&channel=play&country=FR&cv=50581_6.1.2&device=android_26&device_type=android_phone&from_date=2022-05-01&lang=fr_FR&query_type=detail&r=4b5ec474-5b8b-47fb-8c0e-a5bb64139877&t=1655575560695&timezone=Africa%2FDouala&to_date=2022-06-18&v=2.0	'
        headers = {
            'apptoken': auth_info['token_info']['app_token'],
        }
        data = {
            'query_type': 'summary',
            'device_type': 'android_phone',
            'userid': auth_info['token_info']['user_id'],
            'from_date': '2019-01-01',
            'to_date': '2022-06-30',
        }
        response = requests.get(band_data_url, params=data, headers=headers)
        for daydata in response.json()['data']:
            day = daydata['date_time']
            print(day)
            summary = json.loads(base64.b64decode(daydata['summary']))
            for k, v in summary.items():
                if k == 'stp':
                    self.dump_step_data(day, v)
                elif k == 'slp':
                    self.dump_sleep_data(day, v)
                else:
                    print(k, "=", v)

    def mifit_login_with_token(self, login_data):
        login_url = 'https://account.huami.com/v2/client/login'
        data = {
            'app_name': 'com.xiaomi.hm.health',
            'dn': 'account.huami.com,api-user.huami.com,api-watch.huami.com,api-analytics.huami.com,app-analytics.huami.com,api-mifit.huami.com',
            'device_id': '02:00:00:00:00:00',
            'device_model': 'android_phone',
            'app_version': '4.0.9',
            'allow_registration': 'false',
            'third_name': 'huami',
        }

        data.update(login_data)
        response = requests.post(login_url, data=data, allow_redirects=False)
        result = response.json()
        # self.mifit_get_users_patients()
        return result

    def minutes_as_time(self, minutes):
        return "{:02d}:{:02d}".format((minutes//60) % 24, minutes % 60)

    def dump_sleep_data(self, day, slp):
        print("Total sleep: ", self.minutes_as_time(slp['lt']+slp['dp']),
              ", deep sleep", self.minutes_as_time(slp['dp']),
              ", light sleep", self.minutes_as_time(slp['lt']),
              ", slept from", datetime.datetime.fromtimestamp(slp['st']),
              "until", datetime.datetime.fromtimestamp(slp['ed']))

        if 'stage' in slp:
            for sleep in slp['stage']:
                if sleep['mode'] == 4:
                    sleep_type = 'light sleep'
                elif sleep['mode'] == 5:
                    sleep_type = 'deep sleep'
                else:
                    sleep_type = "unknown sleep type: {}".format(sleep['mode'])
                print(format(self.minutes_as_time(sleep['start'])), "-", self.minutes_as_time(sleep['stop']),
                      sleep_type)

    def dump_step_data(self, day, stp):
        print("Total steps: ", stp['ttl'], ", used",
              stp['cal'], "kcals", ", walked", stp['dis'], "meters")
        if 'stage' in stp:
            for activity in stp['stage']:
                if activity['mode'] == 1:
                    activity_type = 'slow walking'
                elif activity['mode'] == 3:
                    activity_type = 'fast walking'
                elif activity['mode'] == 4:
                    activity_type = 'running'
                elif activity['mode'] == 7:
                    activity_type = 'light activity'
                else:
                    activity_type = "unknown activity type: {}".format(
                        activity['mode'])
                print(format(self.minutes_as_time(activity['start'])), "-", self.minutes_as_time(activity['stop']),
                      activity['step'], 'steps', activity_type)

    def fail(self, message):
        raise ValidationError("Error: {}".format(message))

    def mifit_auth_email(self, email, password):
        print("Logging in with email {}".format(email))
        auth_url = 'https://api-user.huami.com/registrations/{}/tokens'.format(
            urllib.parse.quote(email))
        data = {
            'state': 'REDIRECTION',
            'client_id': 'HuaMi',
            'redirect_uri': 'https://s3-us-west-2.amazonws.com/hm-registration/successsignin.html',
            'token': 'access',
            'password': password,
        }
        response = requests.post(auth_url, data=data, allow_redirects=False)
        response.raise_for_status()
        redirect_url = urllib.parse.urlparse(response.headers.get('location'))
        response_args = urllib.parse.parse_qs(redirect_url.query)
        if ('access' not in response_args):
            self.fail('No access token in response')
        if ('country_code' not in response_args):
            self.fail('No country_code in response')

        print("Obtained access token")
        self.login_token = response_args['access'][0]
        self.contry_code = response_args['country_code']
        return self.mifit_login_with_token({
            'grant_type': 'access_token',
            'country_code': self.contry_code,
            'code': self.login_token,
        })

    def mifit_get_users_patients(self, data):
        print(self.app_token)
        print(self.mifitiduser)
        Patient = self.env['iot.utilisateurs']
        info_amis_url='https://api-mifit-us2.huami.com/huami.health.band.userFriend.getFriendList.json?r=fb74e06e-4f0e-4dd6-adc0-10d7037775cc&t=1656606084382&appid=428135909242707968&byteLength=8&callid=1655575560690&channel=play&country=FR&cv=50581_6.1.2&device=android_26&device_type=android_phone&lang=fr_FR&query_type=detail&r=4b5ec474-5b8b-47fb-8c0e-a5bb64139877&t=1655575560695&timezone=Africa%2FDouala&to_date=2022-06-18&v=2.0'
        headers={
		'apptoken': data['token_info']['app_token'],
	    }
        data={
        'query_type': 'summary',
        'device_type': 'android_phone',
        'userid': data['token_info']['user_id'],
        'to_date': fields.Date.today(),
        }
        response=requests.get(info_amis_url,params=data,headers=headers)
        patient_data = response.json()['data']
        for dico in patient_data:
            if Patient.search([('iduser','like',dico['uid'])]):
                continue
            patient_id = Patient.create({
                'iduser': dico['uid'],
                'pseudo': dico['username'] if 'username' in dico.keys() else 'NoPseudo',
                'datenaissance': dico['birthDay'],
                'idmedecin': self.id
            })
            patient_id.state = 'done'

    def mifit_get_history_patients(self, data, patient_id):
        history_url='https://api-mifit-us2.huami.com/huami.health.band.userFriend.getUserFriendInfo.json?r=9c9f5c6b-4184-4232-bba9-5f9ffd94b246&t=1656792549204&appid=428135909242707968&byteLength=8&callid=1655575560690&channel=play&country=FR&cv=50581_6.1.2&device=android_26&device_type=android_phone&lang=fr_FR&query_type=detail&r=4b5ec474-5b8b-47fb-8c0e-a5bb64139877&t=1655575560695&timezone=Africa%2FDouala&v=2.0'
        headers={
		'apptoken': data['token_info']['app_token'],
	    }
        data={
        'query_type': 'summary',
        'device_type': 'android_phone',
        'userid': data['token_info']['user_id'],
        'to_date': fields.Date.today(),
        'from_date': '2022-01-01',
        'f_uid': patient_id
        }
        response=requests.get(history_url,params=data,headers=headers)
        return response.json()['data']


   
